package codeC;

import java.util.ArrayList;
import java.util.LinkedList;

/* code for children's game, using runGame method to the the answer*/
public class Sequence {

    /**
     * The game controller of the children's game:
     *
     * @param k   the counting number "k"
     * @param num the numbers of children in the circle
     * @throws RuntimeException
     */
    public static void runGame(int k, int num) {
        if (!verifyInput(k, num)) {
            throw new RuntimeException("BOTH K AND NUM CANNOT BE ZERO");
        }

        ArrayList<Integer> list = start(k, num);
        System.out.println("the sequence of children as they are eliminated is :");
        Integer winnerId = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        for (Integer id : list) {
            System.out.print(id);
            System.out.print(" ");
        }

        System.out.println();
        System.out.println("the if of the winning child is : " + winnerId);
    }

    /* verify input such that both k and num is not zero*/
    private static boolean verifyInput(int k, int num) {
        return (k != 0 && num != 0);
    }

    /* get the sequence of children as they are eliminated */
    private static ArrayList<Integer> start(int k, int num) {
        int id = 0;
        int length = num;
        LinkedList<Integer> linkedList = createList(num);
        ArrayList<Integer> sequence = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            int outId = getOut(k, num, id);
            int studentId = linkedList.get(outId);
            linkedList.remove(outId);
            id = outId;
            num--;
            sequence.add(studentId);
        }
        return sequence;
    }

    /* create a LinkedList that store the initial student id*/
    private static LinkedList createList(int num) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            list.add(i);
        }
        return list;
    }

    /* algorithm for calculating the student who need to be out at a round*/
    private static int getOut(int k, int num, int id) {
        return ((k - 1) % num + id % num) % num;
    }

}

