package codeC;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.List;

public class BundleCalculator {
    public final static List<Integer> IMG = Arrays.asList(new Integer[]{5, 10});
    public final static List<Integer> FLAC = Arrays.asList(new Integer[]{3, 6, 9});
    public final static List<Integer> VID = Arrays.asList(new Integer[]{3, 5, 9});
    private Map<Integer, Double> imgDetail = new HashMap<>(2);
    private Map<Integer, Double> audioDetail = new HashMap<>(3);
    private Map<Integer, Double> vedioDetail = new HashMap<>(3);

    public BundleCalculator() {
        imgDetail.put(5, 450.00);
        imgDetail.put(10, 800.00);
        audioDetail.put(3, 427.50);
        audioDetail.put(6, 810.00);
        audioDetail.put(9, 1147.50);
        vedioDetail.put(3, 570.00);
        vedioDetail.put(5, 900.00);
        vedioDetail.put(9, 1530.00);
    }

    /* read input from csv file and store them in a HashMap*/
    /* such that <"IMG", "10">*/
    public static HashMap<String, Integer> readInput() {
        /* the input is the address of the csv file */
        String input = "/Users/boxianmo/Desktop/Code/input.csv";
        File file = new File(input);
        HashMap<String, Integer> inputInformation = new HashMap<>();

        try {
            // -read from filePooped with Scanner class
            Scanner inputStream = new Scanner(file);
            while (inputStream.hasNext()) {
                String data = inputStream.nextLine();
                if (data.length() > 1) {
                    String[] s = data.split(" ");
                    inputInformation.put(s[1], Integer.valueOf(s[0]));
                }
            }
            inputStream.close();


        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        return inputInformation;
    }

    /** input is hashMap such that <"IMG", "10">
     * print out the result
     * @param inputInformation
     */
    public static void processInput(HashMap<String, Integer> inputInformation) {
        if (inputInformation.get("IMG") != null) {
            HashMap<Integer, ArrayList<Double>> imgMap = processImg(inputInformation);
            Double sum = getTotal(imgMap);
            System.out.println(inputInformation.get("IMG") + " IMG $" + sum);
            for (Integer i : imgMap.keySet()) {
                System.out.println(" " + imgMap.get(i).get(0) + " x " + i + " $" + imgMap.get(i).get(1));
            }
        }
        if (inputInformation.get("FLAC") != null) {
            HashMap<Integer, ArrayList<Double>> audMap = processAudio(inputInformation);
            Double sum = getTotal(audMap);
            System.out.println(inputInformation.get("FLAC") + " FLAC $" + sum);
            for (Integer i : audMap.keySet()) {
                System.out.println(" " + audMap.get(i).get(0) + " x " + i + " $" + audMap.get(i).get(1));
            }
        }
        if (inputInformation.get("VID") != null) {
            HashMap<Integer, ArrayList<Double>> vidMap = processVID(inputInformation);
            Double sum = getTotal(vidMap);
            System.out.println(inputInformation.get("VID") + " VID $" + sum);
            for (Integer i : vidMap.keySet()) {
                System.out.println(" " + vidMap.get(i).get(0) + " x " + i + " $" + vidMap.get(i).get(1));
            }
        }

    }

    /* calculate the total amount money needed to be paid */
    public static double getTotal(HashMap<Integer, ArrayList<Double>> map) {
        double sum = 0;
        for (Integer i : map.keySet()) {
            ArrayList<Double> list = map.get(i);
            sum += list.get(1);
        }
        return sum;
    }

    /**
     * given the input such as <"IMG", "10">, calculate 1 x 10 $800
     * storing in a HashMap, such that <"10", (1, 800)>
     * @param inputInformation
     * @return
     */
    public static HashMap<Integer, ArrayList<Double>> processImg(HashMap<String, Integer> inputInformation) {
        if (inputInformation.get("IMG") == null) {
            return null;
        }

        ArrayList<Integer> imgList = getBundles(IMG, inputInformation.get("IMG"));
        BundleCalculator bundleCalculator = new BundleCalculator();
        HashMap<Integer, ArrayList<Double>> imgResult = new HashMap<>();

        for (Integer i : imgList) {
            if (imgResult.get(i) == null) {
                ArrayList<Double> count = new ArrayList<>();
                count.add(0, 1.0);
                count.add(1, bundleCalculator.imgDetail.get(i));
                imgResult.put(i, count);
            } else {
                ArrayList<Double> count = imgResult.get(i);
                count.set(0, count.get(0) + 1);
                count.set(1, count.get(0) + bundleCalculator.imgDetail.get(i));
            }
        }

        return imgResult;
    }

    /**
     * given the input such as <"FLAC", "15">, calculate 1 x 9 $1147.50 and 1 x 6 $810
     * storing in a HashMap, such that <"9", (1, 1147.50)> and <"6", (1, 810)>
     * @param inputInformation
     * @return
     */
    public static HashMap<Integer, ArrayList<Double>> processAudio(HashMap<String, Integer> inputInformation) {
        if (inputInformation.get("FLAC") == null) {
            return null;
        }

        ArrayList<Integer> imgList = getBundles(FLAC, inputInformation.get("FLAC"));
        BundleCalculator bundleCalculator = new BundleCalculator();
        HashMap<Integer, ArrayList<Double>> audioResult = new HashMap<>();

        for (Integer i : imgList) {
            if (audioResult.get(i) == null) {
                ArrayList<Double> count = new ArrayList<>();
                count.add(0, 1.0);
                count.add(1, bundleCalculator.audioDetail.get(i));
                audioResult.put(i, count);
            } else {
                ArrayList<Double> count = audioResult.get(i);
                count.set(0, count.get(0) + 1);
                count.set(1, count.get(0) + bundleCalculator.audioDetail.get(i));
            }
        }
        return audioResult;
    }

    /**
     * given the input such as <"VID", "13">, calculate 2 x 5 $1800 and 1 x 3 $570
     * storing in a HashMap, such that <"5", (2, 1800)> and <"3", (1, 570)>
     * @param inputInformation
     * @return
     */
    public static HashMap<Integer, ArrayList<Double>> processVID(HashMap<String, Integer> inputInformation) {
        if (inputInformation.get("VID") == null) {
            return null;
        }

        ArrayList<Integer> vidList = getBundles(VID, inputInformation.get("VID"));
        BundleCalculator bundleCalculator = new BundleCalculator();
        HashMap<Integer, ArrayList<Double>> vedioResult = new HashMap<>();

        for (Integer i : vidList) {
            if (vedioResult.get(i) == null) {
                ArrayList<Double> count = new ArrayList<>();
                count.add(0, 1.0);
                count.add(1, bundleCalculator.vedioDetail.get(i));
                vedioResult.put(i, count);
            } else {
                ArrayList<Double> count = vedioResult.get(i);
                count.set(0, count.get(0) + 1);
                count.set(1, count.get(1) + bundleCalculator.vedioDetail.get(i));
            }
        }
        return vedioResult;
    }

    /**
     * given the total numbers of category, calculate the combinations, such that
     * input VID 13 -> (5, 3)
     * @param category
     * @param num total numbers of category
     * @return list of combinations
     */
    public static ArrayList<Integer> getBundles(List category, int num) {
        Stack<Integer> stack = new Stack<>();
        ArrayList<Integer> visited = new ArrayList<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        int start = num;
        stack.push(start);
        map.put(start, null);

        while (true) {
            if (stack.isEmpty()) {
                break;
            }

            Integer item = stack.pop();
            if (!checkExist(item, visited)) {
                if (isGoal(item, category)) {
                    return getResult(item, findParents(item, map));
                }
                visited.add(item);
            }

            ArrayList<Integer> newItems = getNewItems(item, category);
            setParents(item, newItems, map);
            pushStack(newItems, stack);

        }
        return null;
    }

    /* find out the difference of successive members */
    /* such as <3, 8, 13> -> <3, 5, 5>*/
    public static ArrayList<Integer> getResult(int node, ArrayList<Integer> parents) {
        ArrayList<Integer> result = new ArrayList<>();
        result.add(node);
        if (parents.size() == 0) {
            return result;
        }
        result.add(parents.get(0) - node);

        for (int i = parents.size() - 1; i > 0; i--) {
            result.add(parents.get(i) - parents.get(i - 1));
        }

        return result;
    }

    /* find out node's parents */
    /* such as <3, 8, 13>*/
    public static ArrayList<Integer> findParents(int node, Map<Integer, Integer> map) {
        ArrayList<Integer> parentList = new ArrayList<>();
        int newNode = node;

        while (map.get(newNode) != null) {
            newNode = map.get(newNode);
            parentList.add(newNode);
        }

        return parentList;
    }

    /* set a node's parents */
    /* such as 3's parents is 8 and 8's parent is 13*/
    public static void setParents(int toAdd, ArrayList<Integer> items, Map<Integer, Integer> map) {
        for (Integer item : items) {
            map.put(item, toAdd);
        }
    }

    /* push items in stack */
    public static Stack<Integer> pushStack(List<Integer> newItems, Stack<Integer> stack) {
        for (Integer newItem : newItems) {
            stack.push(newItem);
        }
        return stack;
    }

    /* get new items in the category */
    public static ArrayList<Integer> getNewItems(int num, List<Integer> category) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer item : category) {
            if (num > item) {
                result.add(num - item);

            }
        }
        return result;
    }

    /* check if the item is the goal */
    public static boolean isGoal(int num, List<Integer> category) {
        for (Integer item : category) {
            if (item == num) {
                return true;
            }
        }
        return false;
    }

    /* check exist*/
    public static boolean checkExist(int num, List<Integer> myArray) {
        for (Integer i : myArray) {
            if (i == num) {
                return true;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        HashMap<String, Integer> input = readInput();
        BundleCalculator.processInput(input);
    }

}
